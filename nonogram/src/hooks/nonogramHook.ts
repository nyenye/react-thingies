import React from "react";

import { GridType, CellStatus, Nonogram } from "../types";

export type SetCellStatusFunction = (
  y: number,
  x: number,
  status: CellStatus,
) => void;

function initGrid(
  gridSize: number,
  fromGrid: GridType | undefined = undefined,
): GridType {
  const grid: GridType = [];

  for (let y = 0; y < gridSize; y++) {
    grid.push([]);
    for (let x = 0; x < gridSize; x++) {
      grid[y].push(fromGrid !== undefined ? fromGrid[y][x] : "empty");
    }
  }

  return grid;
}

function useNonogram(nonogram: Nonogram) {
  const [grid, setGrid] = React.useState<GridType>(initGrid(nonogram.size));

  const setCellStatus = React.useCallback(
    (y: number, x: number, status: CellStatus) => {
      setGrid((g) => {
        const newGrid = initGrid(nonogram.size, g);
        newGrid[y][x] = newGrid[y][x] === status ? "empty" : status;
        return newGrid;
      });
    },
    [nonogram],
  );

  const resetGrid = React.useCallback((newGridSize: number) => {
    setGrid(initGrid(newGridSize));
  }, []);

  return { grid, size: nonogram.size, setCellStatus, resetGrid };
}

export { useNonogram };
