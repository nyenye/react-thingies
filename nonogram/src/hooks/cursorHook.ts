import React from "react";

import { PositionType } from "../types";

export type MoveCursorFunction = (dx: number, dy: number) => void;

function useCursor(gridSize: number) {
  const [cursor, setCursor] = React.useState<PositionType>({ x: 0, y: 0 });

  const moveCursor = React.useCallback(
    (dx: number, dy: number) => {
      setCursor((c) => {
        const x = Math.max(0, Math.min(gridSize - 1, c.x + dx));
        const y = Math.max(0, Math.min(gridSize - 1, c.y + dy));

        return {
          x,
          y,
        };
      });
    },
    [gridSize],
  );

  const resetCursor = React.useCallback(() => {
    setCursor({ x: 0, y: 0 });
  }, []);

  return { cursor, moveCursor, resetCursor };
}

export { useCursor };
