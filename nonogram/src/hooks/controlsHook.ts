import React from "react";

import { PositionType } from "../types";

import { MoveCursorFunction } from "./cursorHook";
import { SetCellStatusFunction } from "./nonogramHook";

function useControls(
  cursor: PositionType,
  moveCursor: MoveCursorFunction,
  setCellStatus: SetCellStatusFunction,
) {
  React.useEffect(() => {
    function handleKeyUp(e: KeyboardEvent) {
      const key = e.key.toUpperCase();

      switch (key) {
        case "W":
        case "ARROWUP":
          return moveCursor(0, -1);
        case "S":
        case "ARROWDOWN":
          return moveCursor(0, 1);
        case "A":
        case "ARROWLEFT":
          return moveCursor(-1, 0);
        case "D":
        case "ARROWRIGHT":
          return moveCursor(1, 0);
      }

      switch (key) {
        case " ":
          return setCellStatus(cursor.y, cursor.x, "filled");
        case "X":
          return setCellStatus(cursor.y, cursor.x, "crossed");
      }
    }

    window.addEventListener("keyup", handleKeyUp);

    return () => {
      window.removeEventListener("keyup", handleKeyUp);
    };
  }, [cursor, moveCursor, setCellStatus]);
}

export { useControls };
