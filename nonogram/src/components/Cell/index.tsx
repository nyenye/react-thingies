import React from "react";

import { CellStatus } from "../../types";

import { CrossHair } from "../CrossHair";

import styles from "./index.module.scss";

function Cross() {
  return (
    <div className={styles.cross}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        fill="rgb(224, 61, 40)"
        width="100%"
        height="100%"
      >
        <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
        <path d="M0 0h24v24H0z" fill="none" />
      </svg>
    </div>
  );
}

function getCellStyles(
  index: number,
  size: number,
  status: CellStatus,
): React.CSSProperties {
  return {
    borderRightWidth: index === size - 1 ? 0 : (index + 1) % 5 === 0 ? 2 : 1,
    backgroundColor: status === "filled" ? "black" : "transparent",
  };
}

interface CellProps {
  x: number;
  y: number;
  size: number;
  status: CellStatus;
  hasCursor: boolean;
}

function Cell({ x, y, size, status, hasCursor }: CellProps) {
  return (
    <div
      className={`${styles.container} ${styles.crossed}`}
      data-x={x}
      data-y={y}
      style={getCellStyles(x, size, status)}
    >
      {status === "crossed" && <Cross />}
      {hasCursor && <CrossHair />}
    </div>
  );
}

export { Cell };
