import React from "react";

import { CluesType } from "../../types";
import { CellSize, range } from "../../utils";

import styles from "./index.module.scss";

function getCluesStyles(
  gridSize: number,
  type: "top" | "left",
  longestClue: number,
): React.CSSProperties {
  return {
    flexDirection: type === "top" ? "row" : "column",
    width: type === "top" ? gridSize * CellSize + 2 : longestClue * CellSize,
    height: type === "left" ? gridSize * CellSize + 2 : longestClue * CellSize,
    backgroundColor: "white",

    top: type === "top" ? 0 : undefined,
    left: type === "left" ? 0 : undefined,
  };
}

interface CluesProps {
  clues: CluesType;
  type: "top" | "left";
  longestClue: number;
}

function Clues({ clues, type, longestClue }: CluesProps) {
  const gridSize = clues.length;

  const clueRange = range(longestClue);

  return (
    <div
      className={styles.container}
      style={getCluesStyles(gridSize, type, longestClue)}
    >
      {clues.map((clue, index) => {
        if (type === "top") {
          return (
            <div key={index} className={styles.cluesInColumn}>
              {clueRange.map((_, i) => {
                return (
                  <span key={i} className={styles.clue}>
                    {clue[i] || ""}
                  </span>
                );
              })}
            </div>
          );
        }

        return (
          <div key={index} className={styles.cluesInRow}>
            {clueRange.map((_, i) => {
              return (
                <span key={i} className={styles.clue}>
                  {clue[i] || ""}
                </span>
              );
            })}
          </div>
        );
      })}
    </div>
  );
}

export { Clues };
