import React from "react";

import styles from "./index.module.scss";

function CrossHair() {
  return (
    <div className={styles.container}>
      <div className={styles.topLeft} />
      <div className={styles.topRight} />
      <div className={styles.bottomLeft} />
      <div className={styles.bottomRight} />
    </div>
  );
}

export { CrossHair };
