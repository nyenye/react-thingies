import React from "react";

import { GridType, PositionType } from "../../types";
import { CellSize, range } from "../../utils";

import { Cell } from "../Cell";

import styles from "./index.module.scss";

function getContainerStyles(size: number): React.CSSProperties {
  return {
    width: size * CellSize,
    height: size * CellSize,
  };
}

function getRowStyles(index: number, size: number): React.CSSProperties {
  return {
    borderBottomWidth: index === size - 1 ? 0 : (index + 1) % 5 === 0 ? 2 : 1,
  };
}

interface GridProps {
  grid: GridType;
  size: number;
  cursor: PositionType;
  setCellStatus: Function;
}

function Grid({ grid, size, cursor }: GridProps) {
  const rows = range(size);
  const columns = range(size);

  return (
    <div className={styles.wrapper}>
      <div className={styles.container} style={getContainerStyles(size)}>
        {rows.map((y) => {
          return (
            <div key={y} className={styles.row} style={getRowStyles(y, size)}>
              {columns.map((x) => {
                return (
                  <Cell
                    key={x}
                    x={x}
                    y={y}
                    size={size}
                    status={grid[y][x]}
                    hasCursor={cursor.y === y && cursor.x === x}
                  />
                );
              })}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export { Grid };
