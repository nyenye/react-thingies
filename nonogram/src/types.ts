export type CellStatus = "empty" | "filled" | "crossed";

export type DifficultyType = "easy" | "medium" | "hard";

export type GridType = CellStatus[][];

export type PositionType = { x: number; y: number };

export type CluesType = number[][];

export interface Nonogram {
  size: number;
  topClues: CluesType;
  leftClues: CluesType;
  solution: (0 | 1)[][];
}
