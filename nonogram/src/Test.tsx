import React from "react";
import { PositionType } from "./types";

function Test() {
  const [position, setPosition] = React.useState<PositionType>({ x: 0, y: 0 });

  const movePosition = React.useCallback((dx: number, dy: number) => {
    setPosition((pos) => {
      console.log({ pos });
      return {
        x: pos.x + dx,
        y: pos.y + dy,
      };
    });
  }, []);

  React.useEffect(() => {
    movePosition(1, 0);
    movePosition(1, 0);
  }, [movePosition]);

  return (
    <div>
      {position.x} - {position.y}
    </div>
  );
}

export { Test };
