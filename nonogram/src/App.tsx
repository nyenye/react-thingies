import React from "react";

import { Clues, Grid } from "./components";
import { ControlsHook, CursorHook, NonogramHook } from "./hooks";
import * as Nonograms from "./nonograms";
import { DifficultyType } from "./types";
import { CellSize } from "./utils";

import styles from "./App.module.scss";

function getNonogramStyles(size: number): React.CSSProperties {
  return {
    padding: size * CellSize,
  };
}

function App() {
  const [difficulty, setDifficulty] = React.useState<DifficultyType>("easy");
  const { grid, size, setCellStatus, resetGrid } = NonogramHook.useNonogram(
    Nonograms[difficulty],
  );
  const { cursor, moveCursor, resetCursor } = CursorHook.useCursor(size);
  ControlsHook.useControls(cursor, moveCursor, setCellStatus);

  const longestClue = [
    ...Nonograms[difficulty].topClues,
    ...Nonograms[difficulty].leftClues,
  ].reduce((accum, curr) => {
    if (curr.length > accum) {
      return curr.length;
    }
    return accum;
  }, 0);

  function changeDifficulty(newDifficulty: DifficultyType) {
    resetGrid(Nonograms[newDifficulty].size);
    resetCursor();
    setDifficulty(newDifficulty);
  }

  return (
    <>
      <div className={styles.Nonogram} style={getNonogramStyles(longestClue)}>
        <Clues
          clues={Nonograms[difficulty].topClues}
          type="top"
          longestClue={longestClue}
        />
        <Clues
          clues={Nonograms[difficulty].leftClues}
          type="left"
          longestClue={longestClue}
        />
        <Grid
          grid={grid}
          size={size}
          setCellStatus={setCellStatus}
          cursor={cursor}
        />
      </div>

      <div className={styles.difficultySelector}>
        <button onClick={() => changeDifficulty("easy")}>Easy</button>
        <button onClick={() => changeDifficulty("medium")}>Medium</button>
        <button onClick={() => changeDifficulty("hard")}>Hard</button>
      </div>
    </>
  );
}

export default App;
