function toClassNames(classNames) {
  return classNames.join(" ");
}

export { toClassNames };
