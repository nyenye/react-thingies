import React from "react";
import PropTypes from "prop-types";

import styles from "./index.module.scss";

function Padding({
  size,
  top = size,
  right = size,
  bottom = top,
  left = right,
  children
}) {
  const cssVars = React.useMemo(
    () => ({
      "--pad-top": top,
      "--pad-right": right,
      "--pad-bottom": bottom,
      "--pad-left": left
    }),
    [top, right, bottom, left]
  );

  return (
    <div className={styles.container} style={cssVars}>
      {children}
    </div>
  );
}

Padding.propTypes = {
  size: PropTypes.number,
  top: PropTypes.number,
  right: PropTypes.number,
  bottom: PropTypes.number,
  left: PropTypes.number,
  children: PropTypes.node.isRequired
};

Padding.defaultProps = {
  size: 0
};

export { Padding };
