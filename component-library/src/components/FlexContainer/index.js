import React from "react";
import PropTypes from "prop-types";

import styles from "./index.module.scss";

import { toClassNames } from "../../utils/toClassNames";

function FlexContainer({ flex, direction, main, cross, spacing, children }) {
  const cssVars = {
    "--flex": flex,
    "--direction": direction,
    "--main": main,
    "--cross": cross,
    "--spacing": spacing
  };

  return (
    <div
      className={toClassNames([styles.container, styles[direction]])}
      style={cssVars}
    >
      {children}
    </div>
  );
}

const directionProp = ["row", "column"];

const mainProp = [
  "flex-start",
  "center",
  "flex-end",
  "space-between",
  "space-around",
  "space-evenly"
];

const crossProp = ["flex-start", "center", "flex-end", "stretch", "baseline"];

const wrapProp = ["nowrap", "wrap", "wrap-reverse"];

FlexContainer.propTypes = {
  flex: PropTypes.number,
  direction: PropTypes.oneOf(directionProp),
  main: PropTypes.oneOf(mainProp),
  cross: PropTypes.oneOf(crossProp),
  wrap: PropTypes.oneOf(wrapProp),
  spacing: PropTypes.number,
  children: PropTypes.arrayOf(PropTypes.node).isRequired
};

FlexContainer.defaultProps = {
  flex: 0,
  direction: "row",
  main: "flex-start",
  cross: "flex-start",
  wrap: "nowrap",
  spacing: 0
};

export { FlexContainer };
