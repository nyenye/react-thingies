import React from "react";
import PropTypes from "prop-types";

import styles from "./index.module.scss";

function GridContainer({ spacing, children }) {
  const cssVars = React.useMemo(() => ({ "--spacing": spacing }), [spacing]);

  return (
    <div className={styles.container} style={cssVars}>
      {children}
    </div>
  );
}

GridContainer.propTypes = {
  spacing: PropTypes.number,
  children: PropTypes.node.isRequired
};

GridContainer.defaultProps = {
  spacing: 0
};

export { GridContainer };
