import React from "react";
import PropTypes from "prop-types";

import { toClassNames } from "../../../utils/toClassNames";

import styles from "./index.module.scss";

function GridItem({ children, ...props }) {
  const { xs, sm = xs, md = sm, lg = md, xl = lg } = props;
  const {
    xsOffset,
    smOffset = xsOffset,
    mdOffset = smOffset,
    lgOffset = mdOffset,
    xlOffset = lgOffset
  } = props;

  const { push } = props;

  const cssVars = React.useMemo(
    () => ({
      "--xs": xs,
      "--sm": sm,
      "--md": md,
      "--lg": lg,
      "--xl": xl,
      "--offset-xs": xsOffset,
      "--offset-sm": smOffset,
      "--offset-md": mdOffset,
      "--offset-lg": lgOffset,
      "--offset-xl": xlOffset
    }),
    [xs, sm, md, lg, xl, xsOffset, smOffset, mdOffset, lgOffset, xlOffset]
  );

  return (
    <div
      className={toClassNames([styles.container, push && styles.push])}
      style={cssVars}
    >
      {children}
    </div>
  );
}

GridItem.propTypes = {
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xl: PropTypes.number,
  xsOffset: PropTypes.number,
  smOffset: PropTypes.number,
  mdOffset: PropTypes.number,
  lgOffset: PropTypes.number,
  xlOffset: PropTypes.number,
  push: PropTypes.bool,
  children: PropTypes.node.isRequired
};

GridItem.defaultProps = {
  xs: 12,
  offset: 0,
  push: false
};

export { GridItem };
