import React from "react";
import "./App.css";

import { GridContainer, GridItem, Padding } from "./components/";

function App() {
  return (
    <div className="App">
      <Padding size={1}>
        <GridContainer spacing={1}>
          <GridItem md={6} lg={4} xl={3}>
            <div
              style={{
                backgroundColor: "FireBrick",
                width: "100%",
                height: 100
              }}
            />
          </GridItem>
          <GridItem md={6} lg={4} xl={3}>
            <div
              style={{
                backgroundColor: "LimeGreen",
                width: "100%",
                height: 200
              }}
            />
          </GridItem>
          <GridItem md={6} lg={4} xl={3}>
            <div
              style={{
                backgroundColor: "DarkSlateBlue",
                width: "100%",
                height: 100
              }}
            />
          </GridItem>
          <GridItem md={6} lg={4} xl={3}>
            <div
              style={{
                backgroundColor: "FireBrick",
                width: "100%",
                height: 100
              }}
            />
          </GridItem>
          <GridItem md={6} lg={4} xl={9}>
            <div
              style={{
                backgroundColor: "LimeGreen",
                width: "100%",
                height: 300
              }}
            />
          </GridItem>
          <GridItem md={6} lg={4} xl={2} push>
            <div
              style={{
                backgroundColor: "DarkSlateBlue",
                width: "100%",
                height: 300
              }}
            />
          </GridItem>
        </GridContainer>
      </Padding>
    </div>
  );
}

export default App;
