import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import Styled from "styled-components";

const Container = Styled.div`
  display: flex;
  flex-direction: row;
  min-height: 100%;
  width: 100%;
`;

const Section = Styled.section.attrs(({ size }) => ({
  style: { width: `${size}px` }
}))`min-height: 100%; `;

const Handle = Styled.span`
  display: block;
  min-height: 100%;
  width: 10px;
  background-color: papayawhip;
  cursor: pointer;
`;

function SplitView({ children: [left, right] }) {
  const containerRef = useRef(null);

  const handleRef = useRef(null);

  const [containerSize, setContainerSize] = useState(0);

  const [sectionSizes, setSectionSizes] = useState({ l: 0, r: 0 });

  function onWindowResize(ev) {
    const fullSize = containerRef.current.offsetWidth;
    setContainerSize(fullSize);
  }

  useEffect(() => {
    window.addEventListener("resize", onWindowResize);
    return () => window.removeEventListener("resize", onWindowResize);
  });

  useEffect(() => {
    const handle = handleRef.current;

    function onDrag(ev) {
      setSectionSizes({
        l: ev.x,
        r: containerSize - ev.x - 10
      });
    }

    handle.addEventListener("dragstart", onDrag);
    handle.addEventListener("drag", onDrag);
    handle.addEventListener("dragend", onDrag);

    return () => {
      handle.removeEventListener("dragstart", onDrag);
      handle.removeEventListener("drag", onDrag);
      handle.removeEventListener("dragend", onDrag);
    };
  }, [containerSize, setSectionSizes]);

  useEffect(() => {
    const fullSize = containerRef.current.offsetWidth;
    setContainerSize(fullSize);
  }, [setContainerSize]);

  useEffect(() => {
    setSectionSizes({ l: containerSize / 2 - 5, r: containerSize / 2 - 5 });
  }, [containerSize, setSectionSizes]);

  return (
    <Container ref={containerRef}>
      <Section size={sectionSizes.l}>{left}</Section>
      <Handle ref={handleRef}></Handle>
      <Section size={sectionSizes.r}>{right}</Section>
    </Container>
  );
}

SplitView.propTypes = {
  children: PropTypes.arrayOf((array, index, componentName, _, fullName) => {
    if (array.length !== 2)
      return new Error(
        `Invalid prop ${fullName} supplied to ${componentName}. Must have two children`
      );

    if (
      !array[index].$$typeof ||
      array[index].$$typeof.toString() !== "Symbol(react.element)"
    )
      return new Error(
        `Invalid prop ${fullName} supplied to ${componentName}. Must be of type Element`
      );

    return null;
  }).isRequired
};

export { SplitView };
