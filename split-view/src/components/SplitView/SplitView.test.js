import React from "react";
import { render } from "@testing-library/react";
import { SplitView } from "./SplitView";

test("<SplitView/>", () => {
  const { getByText, container } = render(
    <SplitView>
      <div>Left</div>
      <div>Right</div>
    </SplitView>
  );

  const baseEl = container.firstElementChild;
  expect(baseEl.children.length).toBe(3);

  const leftEl = getByText("Left");
  const rightEl = getByText("Right");
  expect(leftEl.parentElement.nextSibling.nextSibling.firstChild).toBe(rightEl);
});
