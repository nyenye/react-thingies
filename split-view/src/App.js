import React from "react";
import Styled from "styled-components";
import { SplitView } from "./components";

const Container = Styled.div`
  height: 100%;
`;

const Child = Styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  align-items: center;
  font-size: 24px;
  color: white;
`;

const Left = Styled(Child)`
  background-color: palevioletred;
`;

const Right = Styled(Child)`
  background-color: mediumseagreen;
`;

function App() {
  return (
    <Container>
      <SplitView>
        <Left>Left</Left>
        <Right>Right</Right>
      </SplitView>
    </Container>
  );
}

export default App;
